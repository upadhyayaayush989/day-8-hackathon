﻿using FoodApp_Hackathon_Day8.Exceptions;
using FoodApp_Hackathon_Day8.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;

namespace FoodApp_Hackathon_Day8.Repository
{
    internal class FoodRepository : IFoodInterface
    {
        //List<Food> foodItems;
        //List<AvailableLocations> availableLocations;
        //List<Food> foodCart = new List<Food>();
        SqlConnection con = null;
        SqlCommand cmd = null;
        public FoodRepository()
        {
            con = new SqlConnection("Server=DESKTOP-IKB2EQL;Database=foodDb;integrated Security=true");

            //foodItems = new List<Food>()
            //{
            //    new Food(){FoodId=100,FoodName="Veg Shawrma       ",FoodType="Veg",Price=200},
            //    new Food(){FoodId=101,FoodName="Fried Rice        ",FoodType="Veg",Price=300},
            //    new Food(){FoodId=102,FoodName="Tripple Fried Rice",FoodType="Veg",Price=400},
            //    new Food(){FoodId=103,FoodName="Manchurian        ",FoodType="Veg",Price=500},
            //    new Food(){FoodId=104,FoodName="Mix Vegetable     ",FoodType="Veg",Price=600},
            //    new Food(){FoodId=105,FoodName="Biryani           ",FoodType="Veg",Price=450},
            //    new Food(){FoodId=106,FoodName="Burger            ",FoodType="NonVeg",Price=150},
            //    new Food(){FoodId=107,FoodName="Chicken Tikka     ",FoodType="NonVeg",Price=350},
            //    new Food(){FoodId=108,FoodName="BBQ Chicken       ",FoodType="NonVeg",Price=250},
            //    new Food(){FoodId=109,FoodName="Chicken Shawrma   ",FoodType="NonVeg",Price=150}
            //};
            //availableLocations = new List<AvailableLocations>()
            //{
            //    new AvailableLocations(){PinCode=400068},
            //    new AvailableLocations(){PinCode=401105},
            //    new AvailableLocations(){PinCode=401101},
            //    new AvailableLocations(){PinCode=401000},
            //    new AvailableLocations(){PinCode=401102}
            //};
            
        }
        public void AddFoodToCart(string foodId)
        {
           
            int parseFoodId = int.Parse(foodId);
            con.Open();
            string query1 = "select  * from FoodTbl where FoodId = @parseFoodId";
            cmd = new SqlCommand(query1, con);
            cmd.Parameters.AddWithValue("@parseFoodId",parseFoodId);
            SqlDataReader reader = cmd.ExecuteReader();
          
       
            if(reader.Read())
            {
                string query = "insert into FoodCart values (@FoodId,@FoodName,@FoodType,@Price )";
                
                
                SqlCommand cmd2 = new SqlCommand(query, con);
                cmd2.Parameters.AddWithValue("@FoodId", (int)reader["FoodId"]);
                cmd2.Parameters.AddWithValue("@FoodName", (string)reader["FoodName"]);
                cmd2.Parameters.AddWithValue("@FoodType", (string)reader["FoodType"]);
                cmd2.Parameters.AddWithValue("@Price", (int)reader["Price"]);
                Console.WriteLine("Food is added To Cart");
                con.Close();
                con.Open();
                cmd2.ExecuteNonQuery();
                Console.WriteLine("Food is added to the cart ");
                con.Close();
                return;

            }
            con.Close();

            //foreach(Food food in foodItems)
            //{
            //    if (food.FoodId == int.Parse(foodId))
            //    {
            //        foodCart.Add(food);
            //        Console.WriteLine("Food has been added to cart");
            //        return;
            //    }
            //}
            //    Console.WriteLine("The food item is not present ");

        }

        public void PayBill(string userName,int pinCode)
        {

            string query = "select count(*) from FoodCart";
            cmd = new SqlCommand(query, con);
            con.Open();
            int countFood = Convert.ToInt16(cmd.ExecuteScalar());
            con.Close();
            if (countFood == 0)
            {
                Console.WriteLine( "Cart is empty,please order Food!!");
                return;
            }
            query = "select sum(Price) from FoodCart ";
            cmd = new SqlCommand(query, con);
            con.Open();
            int billAmount = Convert.ToInt16(cmd.ExecuteScalar());
            con.Close();
            PrintTotalBill(billAmount, userName, pinCode);
            query = "truncate table FoodCart ";
            cmd = new SqlCommand (query, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            //if (foodCart.Count() == 0)
            //{
            //    Console.WriteLine("Cart is Expty, Please add Some Food Items");
            //    return;
            //}
            //int totalAmount = 0;
            //foreach(Food food in foodCart)
            //{
            //    totalAmount += food.Price;
            //}
            //PrintTotalBill(totalAmount,userName,pinCode);
            //foodCart.Clear();
        }
        private void PrintTotalBill(int totalAmount, string userName, int pinCode)
        {
            Console.WriteLine("********************");
            Console.WriteLine("       Invoice      ");
            Console.WriteLine("********************");
            Console.WriteLine($"Name:    {userName}");
            Console.WriteLine($"PinCode:  {pinCode}");
            Console.WriteLine("                    ");
            string query = "Select * from FoodCart";
            cmd = new SqlCommand(query, con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine($"{reader["FoodName"]}\t\t.......{reader["Price"]} Rs.  \n");
            }
            con.Close();
            //foreach (Food food in foodCart)
            //{
            //    Console.WriteLine($" {food.FoodName} {food.Price}");
            //}
            Console.WriteLine($"\n Total Bill Amount: {totalAmount}Rs.  \n");
            Console.WriteLine($" Date:{DateTime.Now.ToString()}\n");
            Console.WriteLine("************");


        }

        public int foodCartCount()
        {
            string query = "Select Count(*) from FoodCart";
            cmd = new SqlCommand(query, con);
            con.Open();
            int foodcount = Convert.ToInt16(cmd.ExecuteScalar());
            con.Close();
            return foodcount;

        }

        public void RemoveFoodFromCart()
        {

            try
            {
                if (foodCartCount() == 0)
                {
                    throw new CartEmptyException("Cart is Empty , Food Can't be deleted");
                    return;
                }
                ViewCart();
                Console.WriteLine("Enter the FoodId to delete from the Cart:");
                string foodId = Console.ReadLine();
                string query = "select * from FoodCart where FoodId=@foodId";
                cmd = new SqlCommand(query,con);
                cmd.Parameters.AddWithValue("@foodId", int.Parse(foodId));
                con.Open();
                SqlDataReader existFood = cmd.ExecuteReader();
                if (existFood.Read())
                {
                    con.Close();
                    query = "Delete from FoodCart where FoodId=@foodId";
                    cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@foodId", int.Parse(foodId));
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    Console.WriteLine("FoodItem is Deleted successfully!!");
                    return;
                }


                //foreach (Food food in foodCart)
                //{
                //    if (food.FoodId == int.Parse(foodId))
                //    {
                //        foodCart.Remove(food);
                //        return;
                //    }
                //}
                con.Close();
                throw new FoodDeliveryNotAvailableException("Food is not Present in the Cart");
            }
            catch (FoodDeliveryNotAvailableException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (CartEmptyException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool MatchPinCode(int pinCode)
        {
            con.Open();
            string query1 = "Select * from availableLocationsTbl  Where PinCode = @PinCode";
            cmd = new SqlCommand(query1, con);
            cmd.Parameters.AddWithValue("@PinCode", pinCode);
            SqlDataReader reader = cmd.ExecuteReader();
            
            //var varifyPinCode = availableLocations.Where(x => x.PinCode == pinCode).FirstOrDefault();
            if (reader.Read())
            {
                con.Close();
                return true;
            }
            con.Close();
            return false;
        }

        public void UpdateFoodToCart(int foodId)
        {
            throw new NotImplementedException();
        }

        public void ViewCart()
        {
            Console.WriteLine("--Your Cart--");
            string query = "select count(*) from FoodCart";
            cmd = new SqlCommand(query, con);
            con.Open();
            int countFood = Convert.ToInt16(cmd.ExecuteScalar());
            con.Close();
            if (countFood == 0)
            {
                Console.WriteLine("Your Cart is Empty");
            }
            else
            {
                query = "Select * from FoodCart";
                cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine($"{reader["FoodId"]}\t{reader["FoodName"]}\t{reader["FoodType"]}\t{reader["Price"]}");
                }
                con.Close();
                //foreach(Food food in foodCart)
                //{
                //    Console.WriteLine(food);
                //}
            }
        }
        public void ShowMenu()
        {

            string query = "Select * from FoodTbl";
            cmd = new SqlCommand(query,con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine($"{reader["FoodId"]}\t{reader["FoodName"]}\t{reader["FoodType"]}\t{reader["Price"]}");
            }
            con.Close();
            //foreach (Food food in foodItems)
            //{
            //    Console.WriteLine(food);
            //}
        }
    }
}
