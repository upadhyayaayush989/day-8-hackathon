﻿using FoodApp_Hackathon_Day8.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodApp_Hackathon_Day8.Repository
{
    internal interface IFoodInterface
    {
        public void AddFoodToCart(string foodId);

        //public void RemoveFoodFromCart();

        public void UpdateFoodToCart(int foodId);

        public void ViewCart();

        public void PayBill(string userName,int pinCode);
    }
}
