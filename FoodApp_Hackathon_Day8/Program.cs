﻿
using FoodApp_Hackathon_Day8.Repository;
using FoodApp_Hackathon_Day8.Model;

Console.WriteLine("Enter Username:");
string userName = Console.ReadLine();

Console.WriteLine($"------Welcome: {userName}-------");
FoodRepository foodRepository = new FoodRepository();

PincodeCheck:
Console.WriteLine("Enter your PinCode:");
int pinCode = int.Parse(Console.ReadLine());

if (!foodRepository.MatchPinCode(pinCode))
{
    Console.WriteLine("\n!! Sorry our delivery Services is not available\n These are the Locations We are available: 400068,401105,401101,401000, 401102 ");
    goto PincodeCheck;
}



Console.WriteLine("\n#####################################################################");

Console.WriteLine("|Order Food:1 || View Cart:2 ||Remove Cart:3 || Pay Bill:4 || Exit:5 ||");
Console.WriteLine("#######################################################################");


Console.WriteLine($"{userName} Enter Your Choice:");

string choice = Console.ReadLine();

while (choice != "5")
{
    if (choice == "1")
    {
        foodRepository.ShowMenu();
        Console.WriteLine("Enter the Food Id to add Food to cart");
        string foodId = Console.ReadLine();
        foodRepository.AddFoodToCart(foodId);
        foodRepository.ViewCart();

    }
    else if (choice == "2")
    {
        foodRepository.ViewCart();
    }

    else if (choice == "3")
    {
        foodRepository.RemoveFoodFromCart();
    }
    else if(choice == "4")
    {
        foodRepository.PayBill(userName, pinCode);
    }

    Console.WriteLine("\n#####################################################################");
    Console.WriteLine("|Order Food:1 || View Cart:2 ||Remove Cart:3 || Pay Bill:4 || Exit:5 ||");
    Console.WriteLine("#######################################################################");
    choice = Console.ReadLine();

}