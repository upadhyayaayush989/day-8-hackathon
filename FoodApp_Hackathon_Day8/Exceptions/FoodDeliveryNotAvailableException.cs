﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodApp_Hackathon_Day8.Exceptions
{
    internal class FoodDeliveryNotAvailableException:Exception
    {
        public FoodDeliveryNotAvailableException()
        {

        }
        public FoodDeliveryNotAvailableException(string message):base(message)
        {

        }
    }
}
